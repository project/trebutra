<?php

/**
 * @file
 * Administration functions for the Trebutra module.
 */

/**
 * Form constructor for the admin settings form.
 */
function trebutra_settings_form($form, &$form_state) {
  $form['trebutra_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Trello API Key'),
    '#default_value' => variable_get('trebutra_api_key', ''),
    '#description' => t('Please click <a href="https://trello.com/app-key" target="_blank">here</a> to get your key.'),
  );
  $trebutra_api_key = variable_get('trebutra_api_key');
  if(!empty($trebutra_api_key)) {
    $form['trebutra_token'] = array(
      '#type' => 'textfield',
      '#title' => t('Trello API token'),
      '#default_value' => variable_get('trebutra_token', ''),
      '#description' => t('Click "Generate Token" to retrieve your token.'),
    );
    $trebutra_token = variable_get('trebutra_token');
    if(!empty($trebutra_token)) {
      $form['actions']['retrieve_boards'] = array(
        '#type' => 'submit',
        '#value' => t('Sync Boards & Lists'),
        '#submit' => array('trebutra_retrieve_boards'),
      );
      $trebutra_boards = variable_get('trebutra_boards');
      if(!empty($trebutra_boards)) {
        $form['trebutra_listid'] = array(
          '#type' => 'select',
          '#title' => t('Trello Boards'),
          '#options' => variable_get('trebutra_boards', array('error'=>'error: could not retrieve any boards')),
          '#description' => t('Select the board and list you\'d like bugs posted to.'),
          '#default_value' => variable_get('trebutra_listid', ''),
        );
        $form['trebutra_email'] = array(
          '#type' => 'textfield',
          '#title' => t('Report recipient email'),
          '#default_value' => variable_get('trebutra_email', ''),
          '#description' => t('If you would like to receive bug reports, enter an email here.'),
        );
      }
    } else {
      $form['#attached']['js'] = array(drupal_get_path('module', 'trebutra') . '/trebutra.js' => array('type' => 'file', 'scope' => 'footer'));
    }
    $form['actions']['get_token'] = array(
      '#type' => 'submit',
      '#value' => t('Generate Token'),
      '#submit' => array('trebutra_get_token'),
    );
  }
  $form['#submit'][] = 'trebutra_settings_form_submit';


  return system_settings_form($form);
}

function trebutra_settings_form_submit($form, &$form_state) {
  $trebutra_api_key = variable_get('trebutra_api_key');
  if(empty($trebutra_api_key)) {
    drupal_set_message('Great, now you can generate a token.');
  } else {
    $trebutra_boards = variable_get('trebutra_boards');
    if(!empty($trebutra_boards)) {
      drupal_set_message('Success! You\'re ready to go.');
    } else {
      drupal_set_message('Success! Now, retrieve your board lists by clicking "Sync Boards & Lists". If you have a lot of boards - and a lot of lists and cards - this may take several minutes, please be patient.');
    }
  }
}

/**
 * Form constructor for the trebutra label settings form.
 */
function trebutra_label_settings_form($form, &$form_state) {
  $form['trebutra_title_label'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => variable_get('trebutra_title_label', ''),
    '#description' => t('Ex: Summarise your issue in a few words'),
  );
  $form['trebutra_description_label'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#default_value' => variable_get('trebutra_description_label', ''),
    '#description' => t('Ex: Tell us about your problem in more detail'),
  );
  $form['trebutra_email_label'] = array(
    '#type' => 'textfield',
    '#title' => t('Email'),
    '#default_value' => variable_get('trebutra_email_label', ''),
    '#description' => t('Ex: Your email (optional)'),
  );
  $form['trebutra_severity_label'] = array(
    '#type' => 'textfield',
    '#title' => t('Severity'),
    '#default_value' => variable_get('trebutra_severity_label', ''),
    '#description' => t('Ex: Severity'),
  );
  $form['trebutra_severity_allowed_values'] = array(
    '#type' => 'textarea',
    '#title' => t('Severity - Allowed values list'),
    '#default_value' => trebutra_allowed_values_string(variable_get('trebutra_severity_allowed_values', array()))
    ,
    '#rows' => 10,
    '#description' => t('The possible values this field can contain. Enter one value per line, in the format key|label.'),
    '#element_validate' => array('trebutra_allowed_values_setting_validate'),
  );
  return system_settings_form($form);
}

/**
 * Element validate callback; check that the entered values are valid.
 */
function trebutra_allowed_values_setting_validate($element, &$form_state) {
  $values = trebutra_extract_allowed_values($element['#value']);
  if (!is_array($values)) {
    form_error($element, t('Allowed values list: invalid input.'));
  }
  else {
    // Check that keys are valid for the field type.
    foreach ($values as $key => $value) {
      if (drupal_strlen($key) > 255) {
        form_error($element, t('Allowed values list: each key must be a string at most 255 characters long.'));
        break;
      }
    }
    form_set_value($element, $values, $form_state);
  }
}

/**
 * Parses a string of 'allowed values' into an array.
 *
 * @param $string
 *   The list of allowed values in string format described in
 *   trebutra_allowed_values_string().
 * @return
 *   The array of extracted key/value pairs, or NULL if the string is invalid.
 *
 * @see list_allowed_values_string()
 */
function trebutra_extract_allowed_values($string) {
  $values = array();

  $list = explode("\n", $string);
  $list = array_map('trim', $list);
  $list = array_filter($list, 'strlen');

  foreach ($list as $position => $text) {
    $value = $key = FALSE;

    // Check for an explicit key.
    $matches = array();
    if (preg_match('/(.*)\|(.*)/', $text, $matches)) {
      $key = $matches[1];
      $value = $matches[2];
    }
    $values[$key] = $value;
  }

  return $values;
}

/**
 * Generates a string representation of an array of 'allowed values'.
 *
 * This string format is suitable for edition in a textarea.
 *
 * @param $values
 *   An array of values, where array keys are values and array values are
 *   labels.
 *
 * @return
 *   The string representation of the $values array:
 *    - Values are separated by a carriage return.
 *    - Each value is in the format "value|label" or "value".
 */
function trebutra_allowed_values_string($values) {
  $lines = array();
  foreach ($values as $key => $value) {
    $lines[] = "$key|$value";
  }
  return implode("\n", $lines);
}

function trebutra_retrieve_boards($form, &$form_state) {
  if(variable_get('trebutra_token')) {
    $token = variable_get('trebutra_token');
    $key = variable_get('trebutra_api_key');
    $json_results = file_get_contents('https://trello.com/1/members/me/boards?fields=name&key='.$key.'&token='.$token);

    $results = json_decode($json_results);

    foreach($results as $result) {
      $lists = json_decode(file_get_contents('https://api.trello.com/1/boards/'.$result->id.'/lists?cards=open&card_fields=name&fields=name&key='.$key.'&token='.$token));
      foreach($lists as $list) {
        $boards[$list->id] = $result->name.": ".$list->name;
      }
    }

    if(!empty($boards)) {
      variable_set('trebutra_boards', $boards);
    }
  }
}

function trebutra_get_token($form, &$form_state) {
  $key = variable_get('trebutra_api_key');
  drupal_set_message('WARNING: You MUST press "Save Configuration" before continuing.', 'warning');
  drupal_set_message('If you see the token filled out below, you were successful. If not, you will need to get the token yourself, paste it below and click "Save Configuration". Retrieve it here: https://trello.com/1/connect?key='.$key.'&name=Trebutra&response_type=token&scope=read,write&expiration=never');
  $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
  $site_url = $protocol . $_SERVER['HTTP_HOST'] . request_uri();
  $trello_key = $form_state['values']['trebutra_key'];
  $url = "https://trello.com/1/connect?key='.$key.'&name=Trebutra&response_type=token&scope=read,write&expiration=never&return_url=".$site_url;
  drupal_goto($url, array('external' => TRUE));
}
